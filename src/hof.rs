mod hof_old {
    fn higher() -> Box<Fn(i32) -> i32> {
        Box::new(move |i| {
            i + 5
        })
    }

    #[cfg(test)]
    mod tests {
        #[test]
        fn it_works() {
            let lower = super::higher();
            let result = lower(5);

            assert_eq!(result, 10);
        }
    }
}

mod hof_closure {
    fn add_n(n: i32) -> Box<Fn(i32) -> i32> {
        Box::new(move |i| {
            i + n
        })
    }

    #[cfg(test)]
    mod tests {
        #[test]
        fn it_works() {
            let lower = super::add_n(6);
            let result = lower(5);

            assert_eq!(result, 11);
        }
    }
}

mod hof_as_argument {
    fn add_n(n: i32) -> Box<Fn(i32) -> i32> {
        Box::new(move |i| {
            i + n
        })
    }

    fn run_twice(op: Box<Fn(i32) -> i32>) -> Box<Fn(i32) -> i32> {
        Box::new(move |i| {
            let first = op(i);
            op(first)
        })
    }

    #[cfg(test)]
    mod tests {
        #[test]
        fn it_works() {
            let lower = super::run_twice(super::add_n(5));
            let result = lower(5);

            assert_eq!(result, 15);
        }
    }
}

mod hof_impl_trait {
    fn add_n(n: i32) -> impl Fn(i32) -> i32 {
        move |i| {
            i + n
        }
    }

    fn run_twice(op: impl Fn(i32) -> i32) -> impl Fn(i32) -> i32 {
        move |i| {
            let first = op(i);
            op(first)
        }
    }

    fn run_twice_alt<F>(op: F) -> impl Fn(i32) -> i32
        where F : Fn(i32) -> i32 {
        move |i| {
            let first = op(i);
            op(first)
        }
    }

    #[cfg(test)]
    mod tests {
        #[test]
        fn it_works() {
            let lower = super::run_twice(super::add_n(5));
            let result = lower(5);

            assert_eq!(result, 15);
        }
    }
}
