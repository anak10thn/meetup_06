type Input<'a> = &'a str;

type ParserOutput<'a, T> = Result<(T, Input<'a>), String>;

fn take<'a>(len: usize) -> impl Fn(Input<'a>) -> ParserOutput<'a, &'a str> {
    move |input| {
        Ok((&input[0..len], &input[len..]))
    }
}

use std::fmt::Display;

fn char_where<'a, F, S>(f: F, desc: S) -> impl Fn(Input<'a>) -> ParserOutput<'a, char>
where F: Fn(char) -> bool,
      S: Display {
    let parser = take(1);
    move |input| {
        let (first, rest) = parser(input)?;
        let first = first.chars().next();
        match first {
            Some(letter) if f(letter) => Ok((letter, rest)),
            Some(letter) => Err(format!("Expected {} found `{}`", desc, letter)),
            None => Err(format!("Expected {} found end of file", desc))
        }
    }
}

fn map<'a, P, O1, O2, F>(parser: P, f: F) -> impl Fn(Input<'a>) -> ParserOutput<'a, O2>
where P : Fn(Input<'a>) -> ParserOutput<'a, O1>,
F : Fn(O1) -> O2 {
    move |input| {
        let (o1, r1) = parser(input)?;
        let o2 = f(o1);
        Ok((o2, r1))
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let digit = super::char_where(|c| c.is_digit(10), "digit");
        let number = super::map(digit, |c| c.to_digit(10).unwrap());

        let result = number("5");

        assert_eq!(result, Ok((5, "")));
    }
}
