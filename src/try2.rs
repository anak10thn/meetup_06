type Input<'a> = &'a str;
type ParserOutput<'a, T> = Result<(T, Input<'a>), String>;

trait Parser<'a> {
    type Output;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output>;
}

impl<'a, F, O> Parser<'a> for F
where F: Fn(Input<'a>) -> ParserOutput<'a, O> {
    type Output = O;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        self(input)
    }
}

//---------

fn take<'a>(len: usize) -> impl Parser<'a, Output = &'a str> {
    move |input: Input<'a>| {
        Ok((&input[0..len], &input[len..]))
    }
}

use std::fmt::Display;

pub fn char_where<'a, F, S>(f: F, desc: S) -> impl Parser<'a, Output = char>
where F: Fn(char) -> bool,
    S: Display
{
    let parser = take(1);
    move |input| {
        let (first, rest) = parser.parse(input)?;
        let first = first.chars().next();

        match first {
            Some(letter) if f(letter) => Ok((letter, rest)),
            Some(letter) => Err(format!("Expected {} found `{}`", desc, letter)),
            None => Err(format!("Expected {} found end of file", desc))
        }
    }
}

fn map<'a, P, O1, O2, F>(parser: P, f: F) -> impl Parser<'a, Output = O2>
where P: Parser<'a, Output = O1>, F : Fn(O1) -> O2 {
    move |input| {
        let (o1, r1) = parser.parse(input)?;
        let o2 = f(o1);
        Ok((o2, r1))
    }
}

#[cfg(test)]
mod tests {
    use super::Parser;
    
    fn number<'a>() -> impl super::Parser<'a, Output = u32> {
        let digit = super::char_where(|c| c.is_digit(10), "digit");

        super::map(digit, |c| c.to_digit(10).unwrap())
    }

    #[test]
    fn it_works() {
        let parser = number();

        let result = parser.parse("5");

        assert_eq!(result, Ok((5, "")));
    }
}
