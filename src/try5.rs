pub use crate::try4::*;

impl<'a> Parser<'a> for char {
    type Output = char;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        char_where(|c| c == *self, &format!("`{}`", self)).parse(input)
    }
}

impl<'a> Parser<'a> for &'a str
{
    type Output = &'a str;

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        let len = self.len();
        let (o, r) = take(len).parse(input)?;
        if o == *self {
            return Ok((o, r));
        }
        Err(format!("Expected `{}` found `{}`", self, o))
    }
}

pub fn token<'a>(token: char) -> impl Parser<'a, Output = char> {
    token
}

pub fn and<'a, P1, P2, O1, O2>(p1: P1, p2: P2) -> impl Parser<'a, Output = (O1, O2)>
where P1: Parser<'a, Output = O1>,
      P2: Parser<'a, Output = O2>
{
    (p1, p2)
}

pub fn or<'a, P1, P2, O>(p1: P1, p2: P2) -> impl Parser<'a, Output = O>
where P1: Parser<'a, Output = O>,
      P2: Parser<'a, Output = O>
{
    move |input| {
        match p1.parse(input) {
            Ok((o1, r1)) => Ok((o1, r1)),
            Err(_) => p2.parse(input)
        }
    }
}

/*
 We can write it by hand...
impl<'a, P1, P2> Parser<'a> for (P1, P2)
where P1: Parser<'a>,
      P2: Parser<'a> {
    type Output = (P1::Output, P2::Output);

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        let (p1, p2) = self;
        let (o1, r1) = p1.parse(input)?;
        let (o2, r2) = p2.parse(r1)?;
        Ok(((o1, o2), r2))
    }
}
...

impl<'a, P1, P2, P3, P4, P5> Parser<'a> for (P1, P2, P3, P4, P5)
where P1: Parser<'a>,
      P2: Parser<'a>,
      P3: Parser<'a>,
      P4: Parser<'a>,
      P5: Parser<'a> {
    type Output = (P1::Output,
                   P2::Output,
                   P3::Output,
                   P4::Output,
                   P5::Output);

    fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
        let (p1, p2, p3, p4, p5) = self;

        let (o1, r1) = p1.parse(input)?;
        let (o2, r2) = p2.parse(r1)?;
        let (o3, r3) = p3.parse(r2)?;
        let (o4, r4) = p4.parse(r3)?;
        let (o5, r5) = p5.parse(r4)?;
        Ok(((o1, o2, o3, o4, o5), r5))
    }
}
 */

// Or use this clever macro ;)
macro_rules! impl_parser_tuple {
    () => ();
    ($($name:ident)+) => (
        #[allow(non_snake_case)]
        impl <'a, $($name),* > Parser<'a> for ($($name,)*)
        where
            $($name: Parser<'a>),*
        {
            type Output = ($($name::Output,)*);

            fn parse(&self, input: Input<'a>) -> ParserOutput<'a, Self::Output> {
                let ($($name,)*) = self;
                let i = input;
                $(let ($name, i) = $name.parse(i)?;)*
                    let o = ($($name,)*);
                Ok((o, i))
            }
        }
    );
}

impl_parser_tuple!{A B}
impl_parser_tuple!{A B C}
impl_parser_tuple!{A B C D}
impl_parser_tuple!{A B C D E}
impl_parser_tuple!{A B C D E F}
